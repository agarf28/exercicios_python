import pygame
from pygame.locals import *

pygame.int()

# Game loop begins
while True:
    # Code
    # Code
    pygame.display.update()
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

DISPLAYSURF = pygame.display.set_mode((300, 300))
pygame.draw.circle(DISPLAYSURF, BLACK, (200, 50), 30)

color1 = pygame.Color(0, 0, 0) # BLACK
color2 = pygame.Color(255, 255, 255) # Write
color3 = pygame.Color(128, 128, 128) # Grey
color4 = pygame.Color(255, 0, 0)
