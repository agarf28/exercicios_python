x, y = input().split()

x = float(x)
y = float(y)

potencia = x ** y

print("{: .4f}".format(potencia))
