"""
Escreva um programa que leia
um valor em metros e o exiba
convertido em centimetros e
milímetros.
"""
valor_em_metros = int(input('Coloque o valor metros:  '))

centimetros = valor_em_metros * 100
milimetros = valor_em_metros * 1000

print("O valor em centimetros è {} e em milimetros é {}".format(centimetros, milimetros))
