"""
Faça um programa que leia a largura
e a altura de uma parede em metros,
Calcule a sua área e a quantidade
de tinta necessaria para pinta-la,
sabendo que cada litro de tinta
pinta uma área de 2m quadrado.
"""
largura = int(input("Coloque o valor da largura:  "))
altura = int(input('Coloque o valor da altura:  '))

area = largura * altura
quantidade_tinta = area / 2

print('A area da parede é {} e a quantidade de tinta gasta foi de {} litros'.format(area, quantidade_tinta))
