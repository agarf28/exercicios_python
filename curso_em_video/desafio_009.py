"""
Faça um programa que
leia um número inteiro
qualquer e mostre na
tela a sua tabuada.
"""
numero = int(input('Coloque um numero inteiro:  '))
um = 1
dois = 2
tres = 3
quatro = 4
cinco = 5
seis = 6
sete = 7
oito = 8
nove = 9
dez = 10

print('{} x 1 = {}'.format(numero, um))
print('{} x 2 = {}'.format(numero, dois))
print('{} x 3 = {}'.format(numero, tres))
print('{} x 4 = {}'.format(numero, quatro))
print('{} x 5 = {}'.format(numero, cinco))
print('{} x 6 = {}'.format(numero, seis))
print('{} x 7 = {}'.format(numero, sete))
print('{} x 8 = {}'.format(numero, oito))
print('{} x 9 = {}'.format(numero, nove))
print('{} x 10 = {}'.format(numero, dez))
