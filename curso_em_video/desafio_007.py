"""
Desenvolva um programa que leia
as duas notas de um aluno. Calcule
e mostre a sua media.
"""
nota1 = int(input("Coloque o valor da nota 1:  "))
nota2 = int(input("Coloque o valor da nota 2:  "))

media = (nota1 + nota2) / 2

print('A media do aluno é {}'.format(media))
