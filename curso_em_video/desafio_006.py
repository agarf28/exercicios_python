"""
Crie um algoritmo que leia um
número e mostre na tela o seu
dobro, triplo e raiz quadrada.
"""

numero = int(input("E ai, vai colocar um numero. To esperando!  "))
dobro = numero * 2
triplo = numero * 3
raiz_quadrada = numero ** (1/2)

print("O dobro do número é {}, e {} é o seu triplo e sua raiz quadrada é {:.2f}".format(dobro, triplo, raiz_quadrada))
