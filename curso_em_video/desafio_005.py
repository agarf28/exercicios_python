"""
Faça um programa que leia
um numero inteiro e mostre
na tela o seu sucessor e
o seu antecessor.
"""
numero = int(input("Coloque um número:  "))
antecessor = numero - 1
sucessor = numero + 1

print("O sucessor do numero é {} e o antecessor é {}".format(sucessor, antecessor))
